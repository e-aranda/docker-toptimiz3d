#!/bin/sh

# COMMAND=/bin/bash
COMMAND="python3 /home/toptimiz/toptimiz3D/code/toptimiz3d.py"

xhost + # allow connections to X server
docker run --rm --privileged -e "DISPLAY=$DISPLAY" -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v="/home:/mnt/:rw" -i -t toptimiz $COMMAND
