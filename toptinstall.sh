#!/bin/bash

wget https://gitlab.com/e-aranda/topt-mfem/-/archive/master/topt-mfem-master.tar.gz
tar xvfz topt-mfem-master.tar.gz 
cd topt-mfem-master
useradd -m -s /bin/bash toptimiz
HOME=/home/toptimiz
mkdir -p $HOME/Desktop
python3 install.py --cpp="-I/usr/include/suitesparse" --suitesparse --yes

