﻿$IPAddress = Get-WmiObject Win32_NetworkAdapterConfiguration | Where-Object {$_.Ipaddress.length -gt 1} 
Set-Variable -name DISPLAY -Value ($IPAddress.ipaddress[0].ToString() + ":0.0")
docker run --rm --privileged -e "DISPLAY=$DISPLAY" -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v="C:/:/mnt/" -i -t toptimiz python3 /home/toptimiz/toptimiz3D/code/toptimiz3d.py